import { decodeMorze } from '../src/scripts/morze'


test('should be function', () => {
    expect(typeof decodeMorze).toEqual('function');
});

test('should decode \'SOS\' correctly', () => {
    expect(decodeMorze('SOS')).toEqual('...---...');
});

test('should decode \'HEY JUDE\' correctly', () => {
    expect(decodeMorze('HEY JUDE')).toEqual('.....-.--   .---..--...');
});

